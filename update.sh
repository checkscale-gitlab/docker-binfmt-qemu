#!/usr/bin/env bash
set -eo pipefail

cd "$(dirname "$(readlink -f "$BASH_SOURCE")")"

needUpdateCiYml=

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
	versions=( */*/ )
	needUpdateCiYml=yes
fi
versions=( "${versions[@]%/}" )

gitlabCiJobs=
for version in "${versions[@]}"; do
	dist=${version%/*}
	suite=${version#*/}

	echo "$version: $dist/$suite"
	template="Dockerfile.template"
	target="$version/Dockerfile"
	mkdir -p "$(dirname "$target")"
	sed \
		-e 's!@DIST@!'"$dist"'!g' \
		-e 's!@SUITE@!'"$suite"'!g' \
		"$template" > "$target"

	gitlabCiJobs+="test:$suite:"
	gitlabCiJobs+="\n  extends: .test-template"
	gitlabCiJobs+="\n\n"
done

if [ -n "${needUpdateCiYml}" ]; then
  gitlabCi="$(awk -v 'RS=\n\n' '/^## BEGIN test JOBS ##/,/^## END test JOBS ##/ { if ($0 ~ /^## BEGIN/) $0 = "## BEGIN test JOBS ##\n\n'"${gitlabCiJobs}"'## END test JOBS ##"; else next; } { printf "%s%s", $0, RS }' .gitlab-ci.yml)"
  echo "$gitlabCi" > .gitlab-ci.yml
fi
