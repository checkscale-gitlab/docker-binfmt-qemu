include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

variables:
  BUILD_BASE_SERIES: "focal"
  STAGING_IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-"
  IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/binfmt-qemu:"
  IMAGE_VERSION: "1.1"

  REG_SHA256: "ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228"
  REG_VERSION: "v0.16.1"

stages:
  - prerequisites
  - build
  - test
  - deploy
  - cleanup

updated Dockerfiles:
  stage: prerequisites
  image: bash
  script:
    - ./update.sh
    - test -z "$(git status -s)"

build:
  stage: build
  image: docker:git
  services:
    - docker:dind
  before_script:
    - env; set -x
    - echo "${CI_JOB_TOKEN}" |
        docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

    - apk update
    - apk add jq
  script:
    - description=$(wget -q -O - "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq -r .description)
    - for dockerfile in */*/Dockerfile; do
        dir=${dockerfile%/*};
        distro=${dir%/*};
        codename=${dir#*/};

        base_image=;
        case "${distro}" in
        "ubuntu") base_image="registry.gitlab.com/vicamo/docker-brew-ubuntu-core/ubuntu:${codename}" ;;
        esac;

        current_image="${STAGING_IMAGE_PREFIX}${codename}";
        docker build -t "${current_image}"
            ${base_image:+--build-arg "BASE_IMAGE=${base_image}"}
            --label org.opencontainers.image.title="${CI_PROJECT_TITLE}"
            --label org.opencontainers.image.description="${description}"
            --label org.opencontainers.image.authors="You-Sheng Yang"
            --label org.opencontainers.image.url="${CI_PROJECT_URL}"
            --label org.opencontainers.image.documentation="${CI_PROJECT_URL}"
            --label org.opencontainers.image.created="$(date +%Y-%m-%dT%H:%M:%SZ)"
            --label org.opencontainers.image.source="${CI_PROJECT_URL}.git"
            --label org.opencontainers.image.version="${IMAGE_VERSION}.${CI_PIPELINE_IID}"
            --label org.opencontainers.image.revision="${CI_COMMIT_SHA}"
            --label org.opencontainers.image.licenses="Apache-2.0"
            "${dir}";
        docker push --quiet "${current_image}";
      done
  after_script:
    - docker images

.test-template: &test-template
  stage: test
  image: docker:git
  services:
    - docker:dind
  needs:
    - build
  variables:
    DEBIAN_FRONTEND: 'noninteractive'
  before_script:
    - env; set -x
    - echo "${CI_JOB_TOKEN}" |
        docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

    - export JOB_CODENAME="$(echo "${CI_JOB_NAME}" | cut -d ':' -f2)"
  script:
    - lsmod || true
    - mount || true
    - ls -al /proc/sys/fs/binfmt_misc || true
    - docker run --rm --privileged "${STAGING_IMAGE_PREFIX}${JOB_CODENAME}"
    - ls -al /proc/sys/fs/binfmt_misc/qemu-* || true

## BEGIN test JOBS ##

test:bookworm:
  extends: .test-template

test:bullseye:
  extends: .test-template

test:buster:
  extends: .test-template

test:jessie:
  extends: .test-template

test:sid:
  extends: .test-template

test:stretch:
  extends: .test-template

test:bionic:
  extends: .test-template

test:focal:
  extends: .test-template

test:hirsute:
  extends: .test-template

test:impish:
  extends: .test-template

test:jammy:
  extends: .test-template

test:kinetic:
  extends: .test-template

test:trusty:
  extends: .test-template

test:xenial:
  extends: .test-template

## END test JOBS ##

.retrieve-reg-tool: &retrieve-reg-tool |
  wget -O /usr/local/bin/reg "https://github.com/genuinetools/reg/releases/download/${REG_VERSION}/reg-linux-amd64"
  echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
  chmod a+x /usr/local/bin/reg

.deploy-template: &deploy-template
  image: docker:git
  services:
    - docker:dind
  before_script:
    - env; set -x
    - echo "${CI_JOB_TOKEN}" |
        docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

    - i=0;
      image_prefixes="${IMAGE_PREFIX}";
      while true; do
        var=EXTRA_DOCKER_SERVER_${i};
        eval "server=\${${var}:-}";
        var=EXTRA_DOCKER_USER_${i};
        eval "user=\${${var}:-}";
        var=EXTRA_DOCKER_PASSFILE_${i};
        eval "passfile=\${${var}:-}";
        var=EXTRA_DOCKER_REPO_${i};
        eval "repo=\${${var}:-}";
        [ -n "${user}" ] && [ -n "${passfile}" ] && [ -n "${repo}" ] || break;

        cat "${passfile}" | base64 -d |
          docker login --username "${user}" --password-stdin
              ${server:+"${server}"};
        image_prefixes="${image_prefixes} ${repo}:";

        i=$(( i + 1 ));
      done

    - *retrieve-reg-tool

  script:
    - staging_tags=$(
        /usr/local/bin/reg tags --auth-url "${CI_REGISTRY}"
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
            "${STAGING_IMAGE_PREFIX%:*}" |
          grep "^${STAGING_IMAGE_PREFIX##*:}" || true)
    - for alias in oldoldstable oldstable stable testing unstable; do
        codename=$(wget -qO - "http://deb.debian.org/debian/dists/${alias}/Release"
          | awk '/^Codename:/ { print $2 }');
        eval "${alias}_codename=${codename}";
      done
    - eval "$(wget -qO - http://archive.ubuntu.com/ubuntu/dists/devel/Release
        | awk '/^Codename:/ { print "devel_codename=" $2 } /^Version:/ { print "devel_version=" $2 }')"
    - devel_version_year=${devel_version%.*}
    - devel_version_month=${devel_version#*.}
    - devel_version_norm=$((devel_version_year * 12 + devel_version_month))
    - test -z "${staging_tags}" || for staging_tag in ${staging_tags}; do
        codename=$(echo "${staging_tag}" | cut -d- -f2);
        distro=; for d in */${codename}; do distro=${d%/*}; break; done;

        aliases="${codename}";
        for alias in oldoldstable oldstable stable testing unstable devel; do
          eval "alias_codename=\${${alias}_codename}";
          if [ "${codename}" = "${alias_codename}" ]; then
            aliases="${aliases} ${alias}";
            if [ "${alias}" = "stable" ]; then
              aliases="${aliases} latest";
            fi;
          fi;
        done;
        if [ "${distro}" = "ubuntu" ]; then
          eval "version=$({
            wget -qO - http://archive.ubuntu.com/ubuntu/dists/${codename}/Release
              || wget -qO - http://old-releases.ubuntu.com/ubuntu/dists/${codename}/Release; }
            | awk '/^Version:/ {print $2}')";
          aliases="${aliases} ${version}";

          version_year=${version%.*};
          version_month=${version#*.};
          version_norm=$((version_year * 12 + version_month));
          if [ "$((devel_version_norm - version_norm))" = "6" ]; then
            aliases="${aliases} rolling";
          fi;
        fi;

        staging_image="${STAGING_IMAGE_PREFIX%:*}:${staging_tag}";
        echo "\#\#\#\#\# To push ${staging_image} as ${aliases} \#\#\#\#\#";
        if [ "${CI_COMMIT_BRANCH}" != "${CI_DEFAULT_BRANCH}" ]; then
          continue;
        fi;

        docker pull --quiet "${staging_image}";
        for alias in ${aliases}; do
          for prefix in ${image_prefixes}; do
            image="${prefix}${alias}";
            docker tag "${staging_image}" "${image}";
            docker tag "${staging_image}" "${image}-${IMAGE_VERSION}";
            docker push --quiet "${image}";
            docker push --quiet "${image}-${IMAGE_VERSION}";
            docker rmi "${image}";
            docker rmi "${image}-${IMAGE_VERSION}";
          done;
        done;
        docker rmi "${staging_image}";
      done
  after_script:
    - docker images

test:deploy:
  extends: .deploy-template
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

deploy:
  extends: .deploy-template
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

cleanup:
  stage: cleanup
  image: docker
  services:
    - docker:dind
  dependencies: []
  variables:
    GIT_STRATEGY: none
  before_script:
    - *retrieve-reg-tool
  script:
    - staging_tags=$(
        /usr/local/bin/reg tags --auth-url "${CI_REGISTRY}"
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
            "${STAGING_IMAGE_PREFIX%:*}" |
          grep "^${STAGING_IMAGE_PREFIX##*:}" || true)
    - test -z "${staging_tags}" || for staging_tag in ${staging_tags}; do
        /usr/local/bin/reg rm -d --auth-url "${CI_REGISTRY}"
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
            "${STAGING_IMAGE_PREFIX%:*}:${staging_tag}" || true;
      done
  rules:
    - when: always
